#!/bin/bash

export ARCH=arm
export CROSS_COMPILE=../arm-eabi-4.8.4/bin/arm-eabi-
export USE_CCACHE=1

OPTION=$1
VERSION=$2
#cdate=`date +"%m_%d_%Y"`

if [ "${OPTION}" == "nm" ]; then
	echo ""; echo "## Build Air Kernel - Normal"
	echo ${VERSION} > .version
	make slteskt_defconfig; make -j4; sleep 1

	echo "## Creating boot.img for SKT"
	cp arch/arm/boot/zImage mkboot/skt/zImage

	echo ${VERSION} > .version
	make sltektt_defconfig; make -j4; sleep 1

	echo "## Creating boot.img for KT"
	cp arch/arm/boot/zImage mkboot/ktt/zImage

#	echo ${VERSION} > .version
#	make sltelgt_defconfig; make -j4; sleep 1

#	echo "## Creating boot.img for LG U+"
#	cp arch/arm/boot/zImage mkboot/lgt/zImage

#	cd mkboot
#	./img.sh

elif [ "${OPTION}" == "oc" ]; then
	echo ""; echo "## Build Air Kernel -OverClock";
	echo ${VERSION} > .version
	make slteskt_oc_defconfig; make -j4; sleep 1

	echo "## Creating boot.img"
	cp arch/arm/boot/zImage mkboot/skt_oc/zImage

	echo ${VERSION} > .version
	make sltektt_oc_defconfig; make -j4; sleep 1

	echo "## Creating boot.img"
	cp arch/arm/boot/zImage mkboot/ktt_oc/zImage

#	echo ${VERSION} > .version
#	make sltelgt_oc_defconfig; make -j4; sleep 1

#	echo "## Creating boot.img"
#	cp arch/arm/boot/zImage mkboot/lgt_oc/zImage

#	cd mkboot
#	./img-dual.sh

fi


#echo "## Creating ChangeLog textfile"
#git log --pretty=format:"%aN: %s" -n 200 > changelog_$cdate.txt
echo "## Success !!"
